
/********************************************************************************************************************?
/*
This programme read packets from PCAP files and display all the essential data of packet
In this programme BGP,IP,TCP,UDP,ARP,ETHERNET protocol are handled
pcap header files are used for packet handling 
*/

#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SIZE_ETHERNET 14

#define ETHER_ADDR_LEN	6


/********************************************************************************************************************/

struct ethernet_format {
        u_char  ether_dhost[ETHER_ADDR_LEN];    // destination host address 
        u_char  ether_shost[ETHER_ADDR_LEN];    // source host address 
        u_short ether_type;                     // IP? ARP? RARP? etc 
};

/********************************************************************************************************************/

struct ip_format {
        u_char  ip_vhl;                 // version << 4 | header length >> 2 
        u_char  ip_tos;                 // type of service 
        u_short ip_len;                 // total length 
        u_short ip_id;                  // identification 
        u_short ip_off;                 // fragment offset field 
        u_char  ip_ttl;                 // time to live 
        u_char  ip_p;                   // protocol 
        u_short ip_sum;                 // checksum 
        struct  in_addr ip_src,ip_dst;  // source and dest address 
};

#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)

/********************************************************************************************************************/

typedef u_int tcp_seq;

struct tcp_format {
        u_short th_sport;               /* source port */
        u_short th_dport;               /* destination port */
        tcp_seq th_seq;                 /* sequence number */
        tcp_seq th_ack;                 /* acknowledgement number */
        u_char  th_data_off;            /* data offset, rsvd */
        u_char  th_flags;
        u_short th_win;                 /* window */
        u_short th_sum;                 /* checksum */
        u_short th_urp;                 /* urgent pointer */
};

/********************************************************************************************************************/

typedef struct arp
{

u_short hardware_type;              //Hardware Protocol
u_short protocol_type;              //Software protocol Used for ARP
u_char hardware_address_length;     //Hardware Protocol Length
u_char protocol_address_length;      //length of Software protocol Used for ARP
u_short opcode;                      // Message Type
u_char sender_hardware_address[6];
u_char sender_protocol_address[4];
u_char target_hardware_address[6];
u_char target_protocol_address[4];

}arp_format;

/********************************************************************************************************************/

typedef struct udp
{

u_short source_port;             //source port number
u_short destination_port;        // destination port number
u_short udp_length;               // length of protocol
u_short checksum;

}udp_format;

/********************************************************************************************************************/

typedef struct bgp
{

int marker[4];                     
u_short length;
u_char type;
char message[4077];

}bgp_format;

/********************************************************************************************************************/

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void print_payload(const u_char *payload, int len);
void print_hex_ascii_line(const u_char *payload, int len, int offset);
u_short rotate( u_short no);
int handle_udp(udp_format *udp);
void bgp_open(char message[],u_short length);
void handle_bgp(bgp_format *bgp);
int handle_tcp(struct tcp_format *tcp,u_char *packet,int size_ip);
int handle_ip(struct ip_format *ip,u_char *packet);
int handle_arp(arp_format *arp);
int handle_ethernet(struct ethernet_format *ethernet,u_char *packet);
int handle_ethernet(struct ethernet_format *ethernet,u_char *packet);

/********************************************************************************************************************/

void print_hex_ascii_line(const u_char *payload, int len, int offset)
{

int i;
int gap;
const u_char *ch;

	
	printf("%05d   ", offset);
	
	
	ch = payload;
	for(i = 0; i < len; i++) {
		printf("%02x ", *ch);
		ch++;
		
		if (i == 7)
			printf(" ");
	}
	
	if (len < 8)
		printf(" ");
	
	
	if (len < 16) {
		gap = 16 - len;
		for (i = 0; i < gap; i++) {
			printf("   ");
		}
	}
	printf("   ");
	
	
	ch = payload;
	for(i = 0; i < len; i++) {
		if (isprint(*ch))
			printf("%c", *ch);
		else
			printf(".");
		ch++;
	}

	printf("\n");

return;
}

/********************************************************************************************************************/


void print_payload(const u_char *payload, int len)
{

int len_rem = len;
int line_width = 16;			/* number of bytes per line */
int line_len;
int offset = 0;					/* zero-based offset counter */
const u_char *ch = payload;
printf("\n*******************************************************************\n");
	if (len <= 0)
		return;

	
	if (len <= line_width) {
		print_hex_ascii_line(ch, len, offset);
		return;
	}

	
	for ( ;; ) {
		
		line_len = line_width % len_rem;
		
		print_hex_ascii_line(ch, line_len, offset);
		
		len_rem = len_rem - line_len;
		
		ch = ch + line_len;
		
		offset = offset + line_width;
		
		if (len_rem <= line_width) {
			
			print_hex_ascii_line(ch, len_rem, offset);
			break;
		}
	}

return;
}

/********************************************************************************************************************/


u_short rotate( u_short no)
{

u_short no1=no & 0x00ff;
u_short no2=no & 0xff00;
no1=no1<<8;
no2=no2>>8;
no=no & 0x0000;
no=no1+no2;
return no;

}

/********************************************************************************************************************/


/*
UDP packet is handled
*/

int handle_udp(udp_format *udp)
{

printf("\nSrc port: %d\n", ntohs(udp->source_port));
printf("Dst port: %d\n", ntohs(udp->destination_port));

printf("\n Length :%d\n",rotate(udp->udp_length));
printf("\n checksum:%04x\n",rotate(udp->checksum));
printf("\n*******************************************************************");
}

/********************************************************************************************************************/


/*
open message of the BGP contain various field . All are handled here
*/

void bgp_open(char message[],u_short length)
{

u_short my_asp,hold_time;
int i;

printf("\nVersion ofBGP :%d",message[0]); // version of BGP

my_asp=(u_char)message[1];
my_asp=my_asp<<8;
my_asp=my_asp+(u_char)message[2];
printf("\nMy ASP :%d",my_asp);      // Source ASP Identification no

hold_time=(u_char)message[3];
hold_time=hold_time<<8;
hold_time=hold_time+(u_char)message[4];
printf("\nHold Time :%d (sec)",hold_time); // HOLD time

printf("\nBGP Identifier:");
for(i=0;i<4;i++)
{
printf("%d.",(u_char)message[i+5]);
}

printf("\nOptional Parameter Length:%d",message[9]);  // optional parameter length
printf("\n*******************************************************************");
}


/********************************************************************************************************************/


/*
BGP packet is handled and all data is displayed
*/
void handle_bgp(bgp_format *bgp)
{

printf("\n BGP PROTOCOL::\n\n");

printf("\nLength:%d\n\n",rotate(bgp->length)); // length of BGP packet
printf("\nMessage Type::");

// MEssage type of BGP

switch(bgp->type)
{

case 1:printf("\nOpen Message");
bgp_open(bgp->message,bgp->length);   // OPEN message is handled
break;

case 2:printf("\nUpdate Message");
//bgp_update(bgp->message,bgp->length);
break;

case 3:printf("\nNotification Message");
break;

case 4:printf("\nKeepAlive Message");
break;

case 5:printf("\nRoute Refresh Message");
break;

}
printf("\n*******************************************************************");
}

/********************************************************************************************************************/


/*
TCP paket is handled and all data id displayed
*/

int handle_tcp(struct tcp_format *tcp,u_char *packet,int size_ip)
{
int payload_offset;
bgp_format *bgp;
int size_tcp =(tcp->th_data_off>>4)*4; //size of tcp
 
payload_offset=size_tcp;
	              if (size_tcp < 20) 
                           {
		             printf("   * Invalid TCP header length: %u bytes\n", size_tcp);  //check whether TCP packet is valid or not
                           }
            
	
	                printf("Src port: %d\n", ntohs(tcp->th_sport));  //source port number
	                printf("Dst port: %d\n", ntohs(tcp->th_dport));   //destination port number
                        
                        printf("Sequence No: %u",ntohl(tcp->th_seq));   // sequence no
                        
                        if(tcp->th_flags & 0x10)
                        printf("\nAcknoledgment No: %u",ntohl(tcp->th_ack)); //acknoledgment no
                        
                        printf("\nData Offset: %d", (tcp->th_data_off>>4)*4); // offset to the payload
  
if(tcp->th_flags & 0x01)
printf("\n FINISH FLAG SET");
    
if(tcp->th_flags & 0x02)
printf("\n SYN FLAG SET");
  
if(tcp->th_flags & 0x04)
printf("\n RESET FLAG SET");
  
if(tcp->th_flags & 0x08)
printf("\n PUSH FLAG SET");
  
if(tcp->th_flags & 0x10)
printf("\n ACKNOWLEDGEMENT FLAG SET");

if(tcp->th_flags & 0x20)
printf("\n URGENT FLAG SET");
 
if(tcp->th_flags & 0x40)
printf("\n ECN FLAG SET");

if(tcp->th_flags & 0x80)
printf("\n CONGESTION WINDOW REDUCED FLAG SET");

printf("\nCongestion Window Size :%d" ,rotate(tcp->th_win));

printf("\nChecksum : %04x",rotate(tcp->th_sum));

printf("\n*******************************************************************");

/*
to handle BGP packet source or destination port is checked and if it is 179 then it is BGP protocol
*/
if((ntohs(tcp->th_sport)==179 || ntohs(tcp->th_dport)==179) && tcp->th_flags & 0x08 && (tcp->th_flags & 0x01)!=1)
{
bgp=(bgp_format *)(packet+SIZE_ETHERNET+size_ip+size_tcp); // BGP packet is captured
handle_bgp(bgp);                        
}

return payload_offset;	
	              
}

/********************************************************************************************************************/


/*
Handle IP header and display all the content
*/

int handle_ip(struct ip_format *ip,u_char *packet)
{
 struct tcp_format *tcp;  
 udp_format *udp;  
 int payload_offset=0,temp=0, size_payload=0;
 const u_char *payload;
 int size_ip = IP_HL(ip)*4;
  
printf("\n IP PROTOCOL::\n\n");      
 payload_offset=size_ip;
	
      if (size_ip < 20) 
         {
		printf("   * Invalid IP header length: %u bytes\n", size_ip);    // Check whether IP is Valid or NOT
	}
         
printf("\nIP Version: %d",IP_V(ip));    //ip version
         
printf("\nHeader Length: %d",IP_HL(ip)*4); //ip header length
         
printf("\nTotal Length: %d",rotate(ip->ip_len)); // total length of packet excluding ethernet
         
printf("\nIdentification No: %04x", rotate(ip->ip_id)); 
         
ip->ip_off=rotate(ip->ip_off);
         
if((ip->ip_off>>13)==1)
printf("\nMORE FRAGMENT BIT SET");

if((ip->ip_off>>14)==1)
printf("\nDONT'T FRAGMENT BIT SET");

printf("\nTTL : %d",ip->ip_ttl);   // time to live

printf("\nUpper Layer Protocol :%d",ip->ip_p);  // type of upper layer protocol
 	
	
printf("\nFrom: %s\n", inet_ntoa(ip->ip_src));   //source ip address
printf("To: %s\n", inet_ntoa(ip->ip_dst));      // destination ip address
        
printf("\n*******************************************************************");	
	/* determine protocol */	
	
switch(ip->ip_p) 
{
		
case IPPROTO_TCP:
		printf("\nProtocol: TCP\n\n");
                tcp = (struct tcp_format*)(packet + SIZE_ETHERNET + size_ip);  // tcp packet is captured from the whole packet
                temp=handle_tcp(tcp,packet,size_ip);
                payload_offset=payload_offset+temp;
	        break;
		
case IPPROTO_UDP:
	        printf("\nProtocol: UDP\n\n");
                udp = (udp_format*)(packet + SIZE_ETHERNET + size_ip); // udp packet is captured from the whole packet
                temp=handle_udp(udp);
		return;
		
case IPPROTO_ICMP:
		printf("   Protocol: ICMP\n");
		return;
		
default:
		printf("   Protocol: unknown\n");
		return;
	
}


 payload = (u_char *)(packet + SIZE_ETHERNET + payload_offset);  // payload is captured from the whole packet
	
 size_payload = ntohs(ip->ip_len) - (size_ip + temp);
	

	    if (size_payload > 0)
                      {
		       printf("\nPayload (%d bytes):\n", size_payload);
		       print_payload(payload, size_payload);
	              }


return payload_offset;

}

/********************************************************************************************************************/


/*
ARP packet is handled
*/

int handle_arp(arp_format *arp)
{
int i;
int payload_offset=0;

printf("\nARP PACKET::\n\n");

printf("\nHardware Type: %x",rotate(arp->hardware_type));
printf("\nprotocol Type: %x", rotate(arp->protocol_type));

printf("\nHardware address length: %02x", arp->hardware_address_length);
printf("\nProtocol Address length: %02x", arp->protocol_address_length);

printf("\nARP Packet Type::%04x",rotate(arp->opcode) );

switch(rotate(arp->opcode))
{
case 1:printf("APR request");
       break;
case 2:printf("APR Reply");
       break;
case 3:printf("RAPR request");
       break;
case 4:printf("RAPR reply");
       break;
case 5:printf("DRAPR request");
       break;
case 6:printf("DRAPR reply");
       break;
case 7:printf("DRAPR Error");
       break;
case 8:printf("InAPR request");
       break;
case 9:printf("InAPR reply");
       break;
}

printf("\n Sender Hardware Address::");
for(i=0;i<6;i++)
printf("%02x:",arp->sender_hardware_address[i]);

printf("\n target Hardware Address::");
for(i=0;i<6;i++)
printf("%02x:",arp->target_hardware_address[i]);

printf("\nSender Protocol Address:");
for(i=0;i<4;i++)
printf("%d.",arp->sender_protocol_address[i]);

printf("\nTarget Protocol Address:");
for(i=0;i<4;i++)
printf("%d.",arp->target_protocol_address[i]);

payload_offset=(8+(2*arp->hardware_address_length)+(2*arp->protocol_address_length))*4;
printf("\n*******************************************************************");
return payload_offset;

}

/********************************************************************************************************************/


//this function handle the ethernet header and display all the content

int handle_ethernet(struct ethernet_format *ethernet,u_char *packet)
{

int k,payload_offset=0;
struct ip_format *ip;              
arp_format *arp;
char *payload;                    
int size_ip;
int size_tcp;
int size_payload;

printf("\n ETHERNET ::\n\n");  

printf("\nDESTINATION MAC :");
	for(k=0;k<ETHER_ADDR_LEN;k++)
         {
          printf("%02x:",ethernet->ether_dhost[k]);   // Display source mac address
         }
         
printf("\nSOURCE MAC :");
        for(k=0;k<ETHER_ADDR_LEN;k++)
         {
         printf("%02x:",ethernet->ether_shost[k]);     // Display destination mac address
         }

printf("protocol-%d:",ethernet->ether_type);         //protocol type used in upper layer

payload_offset=SIZE_ETHERNET;
printf("\n*******************************************************************");

switch(ethernet->ether_type)
    {
case 8:
         ip = (struct ip_format*)(packet + SIZE_ETHERNET);  //ip header is taken out from packet
         payload_offset=payload_offset+handle_ip(ip,packet);
         break;
case 1544:
          arp=(arp_format*)(packet+SIZE_ETHERNET);
          payload_offset=payload_offset+ handle_arp(arp);
          break;
    }

return payload_offset;	

}

/********************************************************************************************************************/


/*
pcap_loop  fuction call the got packet fuction with 3 Arguments 2.pcap header 3.actual packet
*/

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{

static int count = 1;                   
int k=0,offset_payload;
struct ethernet_format *ethernet;  
pcap_dumper_t *file_save;	

printf("\n*******************************************************************");
printf("\n*******************************************************************");

printf("\nPacket number %d:\n", count);
count++;

ethernet = (struct ethernet_format*)(packet);        //getting only ethernet header from packet
offset_payload=handle_ethernet(ethernet,packet); 

}


/********************************************************************************************************************/


int main()
{

char *device = NULL;			/* capture device name */
char errbuf[PCAP_ERRBUF_SIZE];		/* error buffer */
struct bpf_program fp;		/* The compiled filter expression */
char filter_exp[] = "ip";	/* The filter expression */
bpf_u_int32 mask;		/* The netmask of our sniffing device */
bpf_u_int32 net;		/* The IP of our sniffing device */
pcap_t *handle;			/* packet capture handle */
char file[20];
int num_packets = 10;			/* number of packets to capture */
int choice;



        printf("\n enter the file name::");
        scanf("%s",file);

        handle=pcap_open_offline(file,errbuf);  //to open file handle for capturing packet from file
        pcap_loop(handle, -1, got_packet, NULL); // this actually catch the packet and call got_packet function

        pcap_close(handle);

        printf("\nCapture complete.\n");




return 0;
}

