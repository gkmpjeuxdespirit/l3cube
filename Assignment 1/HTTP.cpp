/*
 * HTTP.cpp
 * Author:Parag Mali(Member Of Team GKMP)
 * June 2014
 * This program is written to display data of http web log file in 
 * meaningful way
 * log file used : weblog.txt 
 */

//Include header files

#include <iostream>
#include<fstream>
#include<string>
#include<string.h>
#include<stdlib.h>
using namespace std;

//Definitions of constants

#define MAX_LINE_SIZE 1500
#define MAX_NUMBER_OF_ENTRIES 3000
#define NOT_FOUND -1
#define MAX_SUBLINES 4
#define MAX_WORD_SIZE 32

//Forward declaration of analyzer class
class Analyzer;

//Format of the log file

class logFormat
{
	friend class Analyzer;
	private:
		string ip;
		string timestamp;
		string method;
		string url;
		string version;
		string responseCode;
		string size;
		string reference;
		string userAgent;
	
	public:
		void display();
		
};

//This function displays the content of entry

void logFormat::display()
{
	cout<<"\n\t\t-----------------------------------------------------------"; 
	cout<<"\n";
	cout<<"\nip       :\t"<<ip;
	cout<<"\ntimestamp:\t"<<timestamp;
	cout<<"\nmethod   :\t"<<method;
	cout<<"\nURL      :\t"<<url;
	cout<<"\nversion  :\t"<<version;
	cout<<"\nresponse :\t"<<responseCode;
	cout<<"\nsize     :\t"<<size<<" in bytes";
	cout<<"\nReferer  :\t"<<reference;
	cout<<"\nuserAgent:\t"<<userAgent;
}

//Analyzer class to analyze log file

class Analyzer
{
	private:
		fstream file;
		logFormat entry[MAX_NUMBER_OF_ENTRIES];
		int numberOfEntries;
		int fieldNumber;
		char line[MAX_LINE_SIZE];
		char subLine[MAX_SUBLINES][MAX_LINE_SIZE];
		const char *delim;
	public:
		Analyzer();
		void analyze();
		void read();
		void parse();
		void store(char* );
		void parseSubLines();
		int findQuote(int);
		void search();
		void displayn(int);
		void uniqueIp();
};

//This function displays nth log 

void Analyzer::displayn(int n)
{
	if(n>numberOfEntries-1 || n<0)
	{
		cout<<"\nNo record";
		return;
	}
	entry[n].display();
}

//Constructor to initialize values

Analyzer::Analyzer()
{
		delim = "[] -";     //Delimiter for tokenization
		numberOfEntries = 0;//number of logs currently processed
}

//This is global search for query
//It searches in all entris in all fields

void Analyzer::search()
{
	int numberOfResults=0;
	string query;
	int count=0;
	cout<<"\nEnter what to search:\t";
	cin>>query;
	for(int i=0;i<numberOfEntries;i++)
	{
		
		if((entry[i].method.find(query))!=string::npos)
		{
			cout<<"\n"<<entry[i].method;	
			cout<<"\nFound at location "<<i<<" in method\n\n";
			count++;
		}
		if(entry[i].version.find(query)!=string::npos)
		{
			cout<<"\n"<<entry[i].version;	
			cout<<"\nFound at location "<<i<<" in version\n\n";
			count++;
		}
		if(entry[i].responseCode.find(query)!=string::npos)
		{
			cout<<"\n"<<entry[i].responseCode;	
			cout<<"\nFound at location "<<i<<" in response code\n\n";
			count++;
		}
		if(entry[i].size.find(query)!=string::npos)
		{
			cout<<"\n"<<entry[i].size;	
			cout<<"\nFound at location "<<i<<" in size\n\n";
			count++;
		}
		if(entry[i].reference.find(query)!=string::npos)
		{
			cout<<"\n"<<entry[i].reference;	
			cout<<"\nFound at location "<<i<<" in reference\n\n";
			count++;
		}
		
		if(entry[i].userAgent.find(query)!=string::npos)
		{
			cout<<"\n"<<entry[i].userAgent;	
			cout<<"\nFound at location "<<i<<" in user agent\n\n";
			count++;
		}
		
		if(count%50 == 0 && count>0)
		{
			numberOfResults = numberOfResults + count;
			count=0;
			cout<<"\nPress any key for next page";
			cin.ignore(1);	
		}
		
	}
	numberOfResults = numberOfResults + count;
	cout<<"\nTotal number of results:\t"<<numberOfResults;
}

//Analyze function to start analyzing the web log file

void Analyzer::analyze()
{
	file.open("weblog.txt",ios::in);
	
	if(file.is_open())
	{
		cout<<"\nAnalyzing file weblog.txt";
		read();		
	}
	else
	{
		cout<<"\nUnable to open file weblog.txt	\nExiting";
		exit(0);
	}
	
	cout<<"\n\n\t\t*****Number of records found:\t"<<numberOfEntries;	
}

//Read function reads the file line by line

void Analyzer::read()
{
	numberOfEntries = 0;	
	file.getline(line,MAX_LINE_SIZE);
	
	while(!file.eof())
	{
		parse();
		file.getline(line,MAX_LINE_SIZE);
	}	
		
}

//Function to find quote within the Line just read 

int Analyzer::findQuote(int location)
{
	while(line[location]!='\0')
	{
		if(line[location]=='"')
		{
			return location;
		}	
		location++;
	}
	return NOT_FOUND;
}

//As strtok function manipulates string,it is better to keep original 
//string as it is and process copies of original string
//Original Line is divided into 4 different parts which are processed 
//separately

void Analyzer::parseSubLines()
{
	char* temp=NULL;
	const char* ldelim=" ";
	temp=strtok(subLine[0],ldelim);
	

	fieldNumber = 4;
	
	while(temp && fieldNumber<7)
	{
		store(temp);
		temp=strtok(NULL,ldelim);
		fieldNumber++;
	}
	
	fieldNumber = 7;
	
	temp=NULL;
	temp=strtok(subLine[1],ldelim);
	
	while(temp && fieldNumber<9)
	{
		store(temp);
		temp=strtok(NULL,ldelim);
		fieldNumber++;
	}
	
}

//Parsing of the line 

void Analyzer::parse()
{
	/**/
	int oLocation=0;
	int cLocation=NOT_FOUND;
	int walker = 0;

	memset(subLine[0],0,MAX_LINE_SIZE);
	memset(subLine[1],0,MAX_LINE_SIZE);
	memset(subLine[2],0,MAX_LINE_SIZE);
	memset(subLine[3],0,MAX_LINE_SIZE);
	for(walker=0;walker<4;walker++)
	{
		oLocation=findQuote(oLocation);
		if(walker==1)
		{
			strncpy(subLine[walker],line+cLocation+1,oLocation-cLocation-1);
		}
		else if(oLocation!=NOT_FOUND)
		{
			cLocation=findQuote(oLocation+1);
			if(cLocation!=NOT_FOUND)
			{
				strncpy(subLine[walker],line+oLocation+1,cLocation-oLocation-1);
			}
			
		}
		oLocation = cLocation +1;
	}
	
	char* temp=NULL;
	temp=strtok(line,delim);
	
	fieldNumber = 1;
	
	while(temp && fieldNumber<3)
	{
		store(temp);
		temp=strtok(NULL,delim);
		fieldNumber++;
	}
	
	entry[numberOfEntries].reference = subLine[2];
	entry[numberOfEntries].userAgent = subLine[3];	
	
	parseSubLines();

	entry[numberOfEntries].display();

	numberOfEntries++;
}

//Store the line in systematic way
//Easier to analyze logs

void Analyzer::store(char* temp)
{
	switch(fieldNumber)
	{
		
	case 1:
			entry[numberOfEntries].ip = temp;
			break;
	case 2:
			entry[numberOfEntries].timestamp = temp;
			break;
	case 4:
			entry[numberOfEntries].method = temp;
			break;
	case 5:
			entry[numberOfEntries].url = temp;
			break;			
	case 6:
			entry[numberOfEntries].version = temp;
			break;
	case 7:
			entry[numberOfEntries].responseCode = temp;
			break;
	case 8:
			entry[numberOfEntries].size = temp;
			break;
	}
}

//This function finds unique ips in log file
//It also prints number of occurences of each ip

void Analyzer::uniqueIp()
{
      int i, j, flag = 1;    // set flag to 1 to start first pass
      logFormat temp;             // holding variable
       
      for(i = 1; (i <= numberOfEntries) && flag; i++)
	  {
          flag = 0;
          for (j=0; j < (numberOfEntries -1); j++)
		  {
               if (entry[j+1].ip.compare(entry[j].ip)>0)      // ascending order simply changes to <
               { 
                    temp = entry[j];             // swap elements
                    entry[j] = entry[j+1];
                    entry[j+1] = temp;
                    flag = 1;               // indicates that a swap occurred.
               }
          }
      }
      
	  int count=1;
	  string currentIp;
	  currentIp = entry[0].ip;
	
      for(i = 1; i < numberOfEntries; i++)
	  {
			
			
			if(currentIp.compare(entry[i].ip))
			{
				cout<<"\n----------------------------------------------";
				cout<<"\nIP:\t"<<currentIp;
				cout<<"\nNumber of occurences:\t"<<count;
				count=1;
				currentIp=entry[i].ip;
			}
			else
			{
				count++;
			}
			
	  }
	  
}
	
//Main function drives the whole program	
//Entry point of program

int main(int argc, char **argv)
{
	int choice;
	Analyzer analyze;
	analyze.analyze();	

	do
	{
		cout<<"\n\n\t\t******Log file analyzer********";
		cout<<"\n1.Display Log file"
			<<"\n2.Display nth log"
			<<"\n3.Query"
			<<"\n4.Find unique ip addresses"
			<<"\n0.Exit";
		cout<<"\nEnter your choice:\t";	
		cin>>choice;
		
		switch(choice)
		{
			case 1:
					analyze.analyze();	
					break;
			case 2:
					int n;
					cout<<"\nEnter record number :\t";
					cin>>n;
					analyze.displayn(n);
					break;
			case 3:
					analyze.search();
					break;
			case 4:
					analyze.uniqueIp();
					break;
			default:
					cout<<"Error:Wrong choice";
		}
		
	}while(choice!=0);
	return 0;
}

