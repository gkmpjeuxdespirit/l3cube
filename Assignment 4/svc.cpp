/*

This Prorgamme Is SIMPLE VERSION CONTROL
We can Create Text file add content to it
Each line shoule have max 10 character and maximum lines in one file are 20.
Also we can delete one line at time from File.
also Can add one line at a time .
I have used the concept of Linked List for this. I have kept linked list of trunks in each version of file and 
tht truck are used for version control
one trunk is one line with max 10 character

*/


#include<iostream.h>
#include<string.h>
#include<fstream.h>
#include<stdlib.h>
#include<conio.h>

/*************************************************************************************************/


class version
{
 
private:

int version_no;
int no_of_lines;
int version_link[20];

public:

version(int version_count,int count,int link[])
{

      version_no=version_count;

      no_of_lines=count;

      for(int i=0;i<count;i++)
      {
      version_link[i]=link[i];
      }

}

/*************************************************************************************************/

void version_entry(char filename[])   // keeping track of version and no of trunks in the version and list of trunk no in the version
{

fstream fp2;
char str2[30];

strcpy(str2,"version");
strcat(str2,filename);

fp2.open(str2,ios::out|ios::app);

fp2<<version_no;
fp2<<"\n";

fp2.close();

char str[30];

strcpy(str,"version_entry_");
strcat(str,filename);

fstream fp;
fp.open(str,ios::out|ios::app);

fp<<version_no;
fp<<"\n";

fp<<no_of_lines;
fp<<"\n";

for(int i=0;i<no_of_lines;i++)
{
fp<<version_link[i];
fp<<" ";
}

fp<<"\n";
fp.close();

}

/*************************************************************************************************/

};

/*************************************************************************************************/


class file_handling
{
private:

fstream file;
char filename[40];

public:

/*************************************************************************************************/

int file_open(char file1[])
{
strcpy(filename,file1);
return 1;
}

/*************************************************************************************************/

void accept_write() // data accepting and writing in file
{

int  link[20];
char str[30];

strcpy(str,"svc_data_");
strcat(str,filename);

fstream svc_data;
svc_data.open(str,ios::out|ios::app);

char line[40];
int count=0;

char choice;

do
{

cout<<"\n Enter the text to be entered in the File (Max 10 character per line and max 20 lines and enter ! to end)::\n";
cin>>line;
	if(strlen(line)<9 && (strcmp(line,"!")!=0))
	{
	   link[count]=count;
	   svc_data<<count;
	   svc_data<<"\n";
	   svc_data<<line;
	   svc_data<<"\n";
	   count++;
	}
	else
	{
	 cout<<"\nSorry Line has more than 9 character\n";
	}


}while((strcmp(line,"!")!=0) && count<20);

svc_data.close();

cout<<"\nDo you want to commit y/n::";
cin>>choice;

if(choice=='y' || choice=='Y')
{
version ver(0,count,link);
ver.version_entry(filename);
write_data(0);
}


}

/*************************************************************************************************/


int get_version_no() // getting new version number
{

char str[30];
int no;

strcpy(str,"version");
strcat(str,filename);

fstream fp;
fp.open(str,ios::in);

while(!fp.eof())
{
fp>>no;
}

fp.close();
return no+1;

}

/*************************************************************************************************/

int get_version_link(int given_version,int line_no[])  // getting link of trunks present in the version
{


int version_no;
int no_of_lines;
char str[30];
int temp;
char line[50];

strcpy(str,"version_entry_");
strcat(str,filename);

fstream fp;
fp.open(str,ios::in);

if(!fp)
{
cout<<"\nError in Opening File\n";
}
int i;

while(!fp.eof())
{

fp>>version_no;
fp>>no_of_lines;


    if(version_no==given_version)
     {
      cout<<"\nfound Version No ::"<<given_version;
      getch();
      break;
     }

fp.getline(line,50);
no_of_lines=-1;
}

i=0;

while(i<no_of_lines)
{

fp>>line_no[i];
i++;

}


fp.close();
return no_of_lines;

}

/*************************************************************************************************/


void write_data(int version)  // writing data to original file and data should be of given version
{

file.open(filename,ios::out);

int no_of_lines;
int line_no[20];
char temp;
int no;

no_of_lines=get_version_link(version,line_no);  // getting trunk list of given version

char line[30];
char str1[30];

strcpy(str1,"svc_data_");
strcat(str1,filename);

fstream svc_data;
svc_data.open(str1,ios::in);

int i=0;
while(i<no_of_lines && !svc_data.eof())   // writing data to the original from file containing all the trunks
{


    svc_data.getline(line,30);

  if(line_no[i]==atoi(line))
    {

      svc_data.getline(line,30);
      file<<line<<"\n";
      i++;
    }
     else
    {
     svc_data.getline(line,30);
    }

}

file.close();
svc_data.close();
}

/*************************************************************************************************/


void show_file()
{
char line[30];
file.open(filename,ios::in);

cout<<"\nFILE CONTENT::\n";

while(!file.eof())
{

file.getline(line,30);
cout<<line<<"\n";

}
file.close();
}

/*************************************************************************************************/

void delete_line(int line_number,int ver)
{

int no_of_lines;
int link[30],link2[20];
no_of_lines=get_version_link(ver,link);
int k=0;

for(int i=0;i<no_of_lines;i++)
{

if(i!=(line_number)-1)
{
link2[k]=link[i];
k++;
}

}

cout<<"\n\n Line Is Deleted From File Version "<<ver<<"\n";

int new_ver=get_version_no();

version vers(new_ver,k,link2);
vers.version_entry(filename);

write_data(new_ver);
}

/*************************************************************************************************/

int get_last_trunk_no()
{
char str[40];
int no;

strcpy(str,"svc_data_");
strcat(str,filename);

fstream svc_data;
svc_data.open(str,ios::in);

char line[40];
char line1[40];
while(!svc_data.eof())
{

svc_data.getline(line1,40);
if(!svc_data)

break;

svc_data.getline(line,40);
no=atoi(line1)+1;
}

svc_data.close();

return no;

}

/*************************************************************************************************/

void add_content(int ver)
{
char str[40];
int no_of_lines;
int link[30],link2[20];
no_of_lines=get_version_link(ver,link);

if(no_of_lines==20)
{
cout<<"\nSorry you Cant Add Line (Max NO.oflines 20)";
return;
}

link[no_of_lines]=get_last_trunk_no();

strcpy(str,"svc_data_");
strcat(str,filename);

fstream svc_data;
svc_data.open(str,ios::out|ios::app);

char line[40];

cout<<"\nEnter The content::\n";
cin>>line;

svc_data<<link[no_of_lines];
svc_data<<"\n";
svc_data<<line;
svc_data<<"\n";
svc_data.close();

cout<<"\n\n Line Is Added And Next Version is Created\n";

int new_ver=get_version_no();

version vers(new_ver,no_of_lines+1,link);
vers.version_entry(filename);

write_data(new_ver);

}

/*************************************************************************************************/

};

/*************************************************************************************************/






int main()
{

file_handling file;
int choice,ver,line_number;
char filename[40];

do
{
cout<<"\n\nEnter the choice\n1.create new file\n2.Open File with appropriate version\n3.Delete content of file of particular version\n4.Add\\ content to file of appropriate version\n5.Show Content Of All Version\n0.Exit\nChoice::";
cin>>choice;

switch(choice)
{
case 1:
       cout<<"\n Enter the file name::";
       cin>>filename;
       cout<<"\nOpening File "<<filename;
       file.file_open(filename);
       file.accept_write();
       cout<<"\n\nFile IS Created\n";
       break;
case 2:
       cout<<"\n Enter the file name::";
       cin>>filename;
       cout<<"\n Enter the version of file::";
       cin>>ver;
       file.file_open(filename);
       file.write_data(ver);
      cout<<"\n/*************************************************************************************************/\n";
       file.show_file();
      cout<<"\n/*************************************************************************************************/\n";
       break;
case 3:
       cout<<"\n Enter the file name::";
       cin>>filename;
       cout<<"\n Enter the version of file::";
       cin>>ver;
       file.file_open(filename);
       file.delete_line(line_number,ver);
       cout<<"\n/*************************************************************************************************/\n";
       file.show_file();
       cout<<"\n/*************************************************************************************************/\n";
       break;
case 4:
       cout<<"\n Enter the file name::";
       cin>>filename;
       cout<<"\n Enter the version of file::";
       cin>>ver;
       file.file_open(filename);
       file.write_data(ver);
       cout<<"\n/*************************************************************************************************/\n";
       file.show_file();
       cout<<"\n/*************************************************************************************************/\n";
       file.add_content(ver);
       cout<<"\n/*************************************************************************************************/\n";
       file.show_file();
       cout<<"\n/*************************************************************************************************/\n";
       break;
case 5:
       cout<<"\n Enter the file name::";
       cin>>filename;
       file.file_open(filename);
       int no=file.get_version_no();
       for(int i=0;i<no;i++)
       {
	 file.write_data(i);
       cout<<"\n/*************************************************************************************************/\n";
       cout<<"\n Version No::"<<i;
       file.show_file();
       cout<<"\n/*************************************************************************************************/\n";
       getch();
       }

}


}while(choice!=0);


}
