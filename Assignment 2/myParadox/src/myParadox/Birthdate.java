package myParadox;

/*
 * class Birthdate
 * date to store date of the month
 * month to store month of the year
 * key stores number between 1 to 366 indicating nth day of year 
 * if key is 366 it is considered as 29th feb    
 * care is taken while generating birth date to make sure that key does not contain invalid value
 */

public class Birthdate {

	private int date;
	private int month;
	private int key;
	
/*
 * getDate()
 * returns date 
 */
	public int getDate() {
		return date;
	}
/*
 * setDate()
 *function to set date
 */
	
	public void setDate(int date) {
		this.date = date;
	}
/*
 * getMonth()
 * returns month	
 */
	
	public int getMonth() {
		return month;
	}
/*
 * setMonth()
 * function to set month	
 */
	
	public void setMonth(int month) {
		this.month = month;
	}
/*
 * 	(non-Javadoc)
 * @see java.lang.Object#toString()
 * function to print Birthdate as string 
 */

	@Override
	public String toString() {
		
		return(" "+date+"/"+month+" ");
		
		
	}
/*
 * getKey()
 * returns the value key
 */
	public int getKey() {
		return key;
	}
/*
 *setKey()
 *returns the value of key 
 */
	public void setKey(int key) {
		this.key = key;
	}
}
