package myParadox;

/*
 * HashTable
 * Hash table has got 366 locations.
 * Collisions are taken care using linked list.
 * Easy to find collision of birth dates. 
 * Hash function : Key value in LinkedHashEntry is used for indexing hash table
 *  
 */

public class HashTable 
{
	public LinkedHashEntry[] table;
	public final static int TABLE_SIZE=366;
	
/*
 * HashTable()
 * Constructor to create new hash table
 * Also this constructor initializes each hash table entry to null
 *  
 */
	public HashTable() {
		
		table = new LinkedHashEntry[TABLE_SIZE];
		
		for(int looper=0;looper<TABLE_SIZE;looper++)
		{
			table[looper] = null;
		}
	}
/*
 * put
 * A function to put entry in hash table
 * if it is first entry then put it directly
 * otherwise,go to the end of the linked list and attach new entry at last	
 * 
 * @param key       key value
 * @param personId  person id
 * 
 * returns void
 */
	public void put(int key,int personId)
	{
		if(table[key]==null)
		{
			table[key]=new LinkedHashEntry(key,personId);
		}
		else
		{
			LinkedHashEntry entry = table[key];
		
			while(entry.getNext()!=null)
			{
				entry = entry.getNext();
			}

			entry.setNext(new LinkedHashEntry(key, personId));
			
		}
	}
}
