package myParadox;
import java.util.Random;

/*
 * RandomBirthdayGenerator
 * Used to generate random number and it is then converted into birth date
 * birth date is of the form date/month or dd/mm without leading zeros
 *   
 */

public class RandomBirthdayGenerator {
	
	private int randomNumber;

/*
 * randomNumberGenerator
 * uses function random to generate random number
 * Absolute value of random number is taken 
 * returns void
 */
	
	private void randomNumberGenerator()
	{
		Random randomGenerator = new Random();
		randomNumber = Math.abs((randomGenerator.nextInt()))%366;
	}

/*
 * birthdate
 * This function converts random number between 0 to 365 to birth date
 * 
 * Number 0 is considered as 29th Feb 
 * Number 1 is 1st of Jan and so on...
 *   	
 */
	
	public Birthdate birthdate()
	{
		Birthdate date;

		randomNumberGenerator();	
		int currentMonth=1;
		date = new Birthdate();	
		date.setKey(randomNumber);
		
		if(randomNumber == 0)
		{
			date.setMonth(2);
			date.setDate(29);
		}
		else
		{
			while(randomNumber>0)
			{
				randomNumber = randomNumber-daysInNextMonth(currentMonth);
				currentMonth = currentMonth + 1;
			}
			currentMonth = currentMonth-1;
			randomNumber = randomNumber+daysInNextMonth(currentMonth);
			
			date.setMonth(currentMonth);
			date.setDate(randomNumber);
		}
		
		return date;
	}
/*
 * daysInNextMonth
 * This function is used by birthdate function to find number of days in currentMonth
 * This function returns number of days in currentMonth
 * Day 0 is considered as 29th Feb 
 * so for Feb it always returns 28 days
 * @param  currentMonth month number 
 * @return int         returns number of days in currentMonth
 * 	
 */
	private int daysInNextMonth(int currentMonth) {
		
		if(currentMonth == 2)
		{
			return 28;
		}
		if(currentMonth == 1 || currentMonth == 3 || currentMonth == 5 || currentMonth == 7 || currentMonth ==8 || currentMonth == 10 || currentMonth == 12 )
		{
			return 31;
		}
		else
		{
			return 30;
		}
	}

	
	
	
}
