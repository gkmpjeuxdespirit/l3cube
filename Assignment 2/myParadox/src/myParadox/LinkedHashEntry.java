package myParadox;

/*
 * LinkedHashEntry
 * This class is used to define type  of entry in hash table
 * Each hash table entry stores person id,key and pointer to next entry in case of collision
 * 
 */

public class LinkedHashEntry {
	

	private int personId;
	private LinkedHashEntry next;
	private int key;
/*
 * LinkedHashEntry
 * A parameterized constructor
 * it initializes key,personId with values passed as parameters 
 * next link is initialized to null
 * 
 * @param key key value
 * @param value person id
 * 
 */
	 LinkedHashEntry(int key, int value) {

         this.key = key;

         this.personId = value;

         this.next = null;

   }
/*
 * getNext
 * returns next entry 
 */
	public LinkedHashEntry getNext() {
		return next;
	}
/*
 * setNext 
 * sets next entry 
 * @param next this points to next entry
 */
	public void setNext(LinkedHashEntry next) {
		this.next = next;
	}
/*
 * getKey
 * returns key
 */
	public int getKey() {
		return key;
	}

/*
 * getPersonId
 * returns personId
 */

	public int getPersonId() {
		return personId;
	}

}
