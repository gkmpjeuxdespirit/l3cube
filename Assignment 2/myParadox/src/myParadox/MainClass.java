package myParadox;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * MainClass
 * This class drives the whole program
 * Two ways to check the correctness of birthday paradox 
 * 1.Manual 
 * 2.Automatic
 *
 */

public class MainClass {

	public static HashTable hashedBirthdates;
	static Birthdate birthdates[];
	static double theoreticalProbability;
	static int numberOfSuccessfulTrials;
	static int numberOfTrials;
	static Scanner in = new Scanner(System.in);

/*
 * main
 * Provides options for manual trials or automatic trials 
 * 
 * @param argv for command line arguments 
 * returns void
 *  
 */
	
	public static void main(String[] argv) {
		
		int choice;
		
		System.out.println("Choose :\n 1.Manual Checking \n 2.Automatic Checking");
		
		choice=in.nextInt();
		
		switch(choice)
		{
			case 1:
				manual();
				break;
			case 2:
				automatic();
				break;
			default:
				System.out.println("Error:Wrong Choice");
		}
		in.close();
	}
/*
 * automatic
 * This method accepts maximum number of people and number of trials for each people count 
 * For each people count this method asks each and every person his/her birth date and 
 * arranges birth dates in hash table.This is performed numberOfTrials times for each people count.
 * Theoretical and actual probability for each person count is stored 
 * In the end,theoretical and actual probability for each person count are compared.
 * Actual error and %error are calculated.  
 * More number of experiments results in more accurate results.
 * This method proves the birthday paradox is indeed correct.
 * 
 * returns void
 * proves the correctness of birthday paradox
 */
	private static void automatic() {
		
		int repeat=0;
		int maxNumberOfPeople=0;
		boolean flag;
		double lobservedProbability[];
		double ltheoreticalProbability[];
		DecimalFormat displayFormat = new DecimalFormat("#.000000000");
		
		System.out.println("\nProving that birthday paradox is indeed correct.");
		System.out.println("\nLarge number of trials will give more accurate results.(At least 200)");
		
		do
		{
			flag=true;
			
			System.out.println("\nEnter the maximum number of people : ");
			try
			{
				maxNumberOfPeople=in.nextInt();
				if(maxNumberOfPeople<0)
				{
					System.out.println("Error:Incorrect number of people.");
					flag = false;
				}
			}
			catch(InputMismatchException err)
			{
				flag=false;
				System.out.println("Error:Input mismatch or out of range.Please try smaller integer values");
			}
		}while(flag!=true);
		
		do
		{
			flag=true;
			System.out.println("\nEnter the number of trials for each people count : ");
	
			try
			{
				repeat=in.nextInt();
				if(repeat<0)
				{
					System.out.println("Error:Incorrect input.");
					flag=false;
				}
			}
			catch(InputMismatchException err)
			{
				flag=false;
				System.out.println("Error:Input mismatch or out of range.Please try smaller integer values");
			}
		}while(flag!=true);
		
		lobservedProbability = new double[maxNumberOfPeople];
		ltheoreticalProbability = new double[maxNumberOfPeople];
		
		lobservedProbability[0]=0;
		lobservedProbability[1]=0;
		ltheoreticalProbability[0]=0;
		ltheoreticalProbability[1]=0;
		
		int repeater;
		int looper;
		double error=0;
		
		for(looper=2;looper<maxNumberOfPeople;looper++)
		{
			repeater = repeat;	
			numberOfSuccessfulTrials=0;
			numberOfTrials=0;
			
			calculateTheoroticalProbability(looper);
			ltheoreticalProbability[looper] = theoreticalProbability;
			
			while(repeater>0)
			{
				askBirthdates(looper);
				display();
				repeater--;
			}
			
			lobservedProbability[looper]= ((double)(numberOfSuccessfulTrials)/(double)numberOfTrials);
		
		}
		for(looper=0;looper<maxNumberOfPeople;looper++)
		{
			System.out.println("\nResults for group of "+looper+" people,after performing "+numberOfTrials+" trials\n");
			System.out.println("Theorotical = " + displayFormat.format(ltheoreticalProbability[looper]) +"\tObserved = "+ displayFormat.format(lobservedProbability[looper]));
			error = error + Math.abs(ltheoreticalProbability[looper] - lobservedProbability[looper]);	
		}
		
		System.out.println("\nThe error is " + error/maxNumberOfPeople);
		System.out.println("\nThe percentage error is " + error*100/maxNumberOfPeople);		
		
		System.out.println("\nAfter seeing results,we can say that,Birthday paradox is indeed correct");
		
	}

/*
 * manual
 * Provides a way to check each and every random experiment performed 
 * Number of people is input 
 * Birth dates are asked to each and every person present.
 * Then these birth dates are arranged in a hash table.
 * After each trial user is asked whether he/she wants to perform more trials
 * Based on number of trials results vary 
 * returns void
 * 	
 */
	
	private static void manual() {
			

		int repeat;
		int numberOfPeople=0;
		boolean flag;
		numberOfSuccessfulTrials=0;
		numberOfTrials=0;
		
		do
		{
		flag=true;
		System.out.println("Enter number Of people :");
		
		try
		{
			numberOfPeople=in.nextInt();
			if(numberOfPeople<0)
			{
				flag=false;
			}
		}
		catch(InputMismatchException err)
		{
			flag=false;
			System.out.println("Error:Input mismatch or out of range.Please try smaller integer values");
			//System.exit(0);
		}
		}while(flag!=true);
		
		do
		{
			repeat = 0;
			askBirthdates(numberOfPeople);
			
			display();
			
			System.out.println("\nDo you want to perform another experiment with same number of people?\n1.Yes\nOther keys.No");
			repeat = in.nextInt();
			
		}while(repeat==1);
		
	}
/*
 * askBirthdates
 * This method asks each and every person present in a room his/her birth date 
 * and arranges in hash table.
 * This method uses RandomBirthdateGenerator class
 * @param numberOfPeople is used to store number of people present in a room.
 * returns void
 * 
 */
	
	private static void askBirthdates(int numberOfPeople) {
		
		
		
		System.out.println(numberOfPeople + " people gathered in a room.");
		calculateTheoroticalProbability(numberOfPeople);
		
		birthdates = new Birthdate[numberOfPeople];
		hashedBirthdates = new HashTable();
		
		RandomBirthdayGenerator generate = new RandomBirthdayGenerator();
		
		System.out.println("\nAsking birthdate to indivisual...\n");
		
		for(int looper = 0;looper<numberOfPeople;looper++)
		{
			birthdates[looper]= generate.birthdate();
			System.out.println("Birthdate of person"+ looper +" is :" + birthdates[looper]);
			hashedBirthdates.put(birthdates[looper].getKey(),looper);
		}
		
		System.out.println("\nBirthdates arranged!!");
		
		
	}

/*
 *calculateTheoroticalProbability 
 *This method calculates theoretical probability that at least two people in a group 
 *of have same birth dates
 *
 * @param numberOfPeople is used to store number of people present in a room.
 * returns void
 */
	
	
	private static void calculateTheoroticalProbability(int numberOfPeople)
	{
		double number=(double)364/365;
		double power;
		
		power = (numberOfPeople)*(numberOfPeople-1)/2;
		
		number = Math.pow(number,power);
		theoreticalProbability = 1-number;
		
	}

/*
 * display
 * This method checks the hash table and displays the output of experiment 
 * This method also keeps track of numberOfTrials performed and numberOfSuccessful trials
 * 
 * return void 		
 */
	public static void display()
	{
		int count;	
		int numberOfRepeatedBirthdays=0;
		numberOfTrials = numberOfTrials + 1;
		for(int looper=0;looper<HashTable.TABLE_SIZE;looper++)
		{
			if(hashedBirthdates.table[looper]!=null)
			{
				LinkedHashEntry entry = hashedBirthdates.table[looper];

				System.out.println("PersonId having birthdate " + birthdates[entry.getPersonId()]);
				count=0;
				while(entry!=null)
				{
					System.out.println(" "+entry.getPersonId()+" ");
					entry = entry.getNext();
					count = count + 1;
				}
				
				if(count>1)
				{
					numberOfRepeatedBirthdays = numberOfRepeatedBirthdays+1;
				}
			}
	
		}
		System.out.println("\nnumber of repeated birthdates :" + numberOfRepeatedBirthdays);
		if(numberOfRepeatedBirthdays>0)
		{
			numberOfSuccessfulTrials = numberOfSuccessfulTrials + 1;
			System.out.println("\nEveryone is suprised after seeing the results!!!");
		}
		
		System.out.println("No one ever thought that there is "+ theoreticalProbability +" theoretical probability that two people in our group have same birthdate"); 
		System.out.println("Number of trials performed = " + numberOfTrials );
		System.out.println("Number of successful trials = " + numberOfSuccessfulTrials);
		System.out.println("\nObserved Probability = "+ ((double)(numberOfSuccessfulTrials)/numberOfTrials));
		
	}
}
