VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   WindowState     =   2  'Maximized
   Begin VB.CommandButton delete_file 
      Caption         =   "Delete duplicate file"
      BeginProperty Font 
         Name            =   "Monotype Corsiva"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   11040
      TabIndex        =   4
      Top             =   8280
      Width           =   3615
   End
   Begin VB.ListBox List3 
      Columns         =   1
      Height          =   6690
      Left            =   10560
      TabIndex        =   1
      Top             =   960
      Width           =   4215
   End
   Begin VB.ListBox List1 
      Columns         =   2
      Height          =   8055
      Left            =   960
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   960
      Width           =   6495
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Duplicate file"
      BeginProperty Font 
         Name            =   "Monotype Corsiva"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10560
      TabIndex        =   3
      Top             =   240
      Width           =   2415
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Total File"
      BeginProperty Font 
         Name            =   "Monotype Corsiva"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   2
      Top             =   240
      Width           =   5895
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'GLobal variables for file access
Dim fso
Dim ObjFolder
Dim count1 As Integer
Dim ObjSubFolder
Dim ObjFiles
Dim objfile
Dim ObjOutFile


Private Sub delete_file_Click()
 Dim flag As Integer
 flag = 0
 Dim k As Integer
 k = 0
 
 Dim temp, temp2 As String
 
 'Variables required to access file system,directories
 
 Dim FileSystemObject, FileObject As Object
 Set FileSystemObject = CreateObject("Scripting.FileSystemObject")
 
 filename = List3.List(List3.ListIndex)  'File to be deleted stored in filename
 'Example l3_assign.txt | (size) ::G:\l3\Assignment\l3_assign.txt
 
 Dim ind As Integer
 ind = List3.ListIndex     'Index is stored
 
 
 If (filename = "") Then
 MsgBox "Please select file to be deleted form listbox"    'If file not selected
 
 Else
 'Loop to separate file name and file path
 While (flag <> 2)
 
    temp = Left(filename, k)  'to retrieve file name fom left
    
    temp2 = Right(temp, 1)  'to get rightmost element of string
    
    If (temp2 = ":") Then   'when : is found  file path starts
    flag = flag + 1
    Else
    
    End If
    
    k = k + 1
    Wend
    
    k = (Len(filename) - Len(temp))   'to store index  of start of file path
 
Set FileObject = FileSystemObject.GetFile(Right(filename, k))   'To get file path which is to be deleted
On Error GoTo Ne
 FileObject.Delete 'File deleted
 GoTo ne2
Ne:
 MsgBox " Permission Denied"
 GoTo ne3
ne2:
 MsgBox "File is deleted"
 List3.RemoveItem (ind)  'File removed from list box
 End If
ne3:
End Sub


Private Sub Form_Load()
Dim j As Integer

count1 = 0


  'Creating File System Object
 Set fso = CreateObject("Scripting.FileSystemObject")
 'Create an output file
Set ObjOutFile = fso.CreateTextFile("OutputFiles.csv")
  'Writing CSV headers
ObjOutFile.WriteLine ("Type,File Name,File Path,Size")
 'Call the GetFile function to get all files
 GetFiles ("G:gbc")
 
  'Close the output file
 ObjOutFile.Close
  search_file
 
  End Sub

Function GetFiles(FolderName)
On Error Resume Next

Dim i As Integer
i = 0
Dim j As Integer
j = 0
    
    Set ObjFolder = fso.GetFolder(FolderName)
    Set ObjFiles = ObjFolder.Files
Dim s As String

'Write all files to output files
For Each objfile In ObjFiles

ObjOutFile.WriteLine ("File," & objfile.Name & "," & objfile.Path & "," & objfile.Size)
List1.AddItem UCase(objfile.Name) & "|" & objfile.Size & " ::" & objfile.Path



   Next

    'Getting all subfolders
    Set ObjSubFolders = ObjFolder.SubFolders
    
        For Each ObjFolder In ObjSubFolders
       
        'Writing SubFolder Name and Path
    'ObjOutFile.WriteLine ("Folder," & ObjFolder.Name & "," & ObjFolder.Path)
'    List2.AddItem ObjFolder.Name

'Getting all Files from subfolder
GetFiles (ObjFolder.Path)
    Next

 End Function

'To search for the duplicate file

Function search_file()
Dim i As Integer
i = 0
Dim j As Integer
Dim filename, str2 As String
Dim temp1, temp2, temp3 As String
Dim flag3 As Integer
flag3 = 0
Dim k, l, flag, flag1, m As Integer
k = 0
l = 0
flag = 0
flag1 = 0

For i = 0 To List1.ListCount - 1    'To search for every file in the listbox

    flag = 0
    flag3 = 1
    flag1 = 0
    k = 0
    j = 0
    
    filename = List1.List(i)        'Store file name ,size,path in filename variable
    If filename = "" Then
    flag1 = 2
    End If
    
    'Separate file name and size from filename variable
    While (flag1 <> 2)
    temp1 = Left(filename, k)
    temp2 = Right(temp1, 1)
    If (temp2 = ":") Then
    flag1 = flag1 + 1
    End If
    k = k + 1
    Wend
    
    'Search for the file from start to end
    
    While (flag <> 1)
  str2 = List1.List(j)
  
  flag2 = 0
  m = 0
  temp3 = ""
  temp2 = ""
  If (str2 = "") Then
  flag2 = 2
  End If
  
  'Separate file name and size from filename
  While (flag2 <> 2)
    temp3 = Left(str2, m)
    temp2 = Right(temp3, 1)
    If (temp2 = ":") Then
    flag2 = flag2 + 1
    End If
    m = m + 1
    Wend
    
 
    
    If (j > List1.ListCount) Then
    flag = 1
    End If
    
    If (temp1 = temp3) And j <> i Then  'If files are same
    List3.AddItem List1.List(i)  'Duplicate file is added in listbox
    flag = 1
    End If
    
    j = j + 1
    Wend
    
Next i



End Function




