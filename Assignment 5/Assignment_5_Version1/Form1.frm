VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   WindowState     =   2  'Maximized
   Begin VB.CommandButton delete_file 
      Caption         =   "Delete Duplicate file"
      BeginProperty Font 
         Name            =   "Monotype Corsiva"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3720
      TabIndex        =   4
      Top             =   9840
      Width           =   4215
   End
   Begin VB.ListBox List4 
      Columns         =   2
      Height          =   255
      Left            =   10320
      TabIndex        =   3
      Top             =   5760
      Visible         =   0   'False
      Width           =   135
   End
   Begin VB.ListBox List3 
      Columns         =   1
      Height          =   7665
      Left            =   480
      TabIndex        =   2
      Top             =   1680
      Width           =   8175
   End
   Begin VB.ListBox List2 
      Height          =   255
      Left            =   2640
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   4440
      Width           =   255
   End
   Begin VB.ListBox List1 
      Columns         =   2
      Height          =   8055
      Left            =   11520
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   1560
      Width           =   6495
   End
   Begin VB.Label Label2 
      Caption         =   "Total Files"
      BeginProperty Font 
         Name            =   "Monotype Corsiva"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10920
      TabIndex        =   6
      Top             =   360
      Width           =   5055
   End
   Begin VB.Label Label1 
      Caption         =   "Duplicate Files"
      BeginProperty Font 
         Name            =   "Monotype Corsiva"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   5
      Top             =   480
      Width           =   5055
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim fso
 Dim ObjFolder
Dim count1 As Integer
 Dim ObjSubFolder
 
    Dim ObjFiles
    Dim objfile
 Dim ObjOutFile

'For delete duplicate files
'File must be selected from the listbox to remove

Private Sub delete_file_Click()
 Dim fg As Integer
 fg = 0
 Dim k As Integer
 k = 0
 
 Dim temp, temp2 As String
 
 'Variables required to access file system,directories
 
 Dim FileSystemObject, FileObject As Object
 Set FileSystemObject = CreateObject("Scripting.FileSystemObject")
 
 str1 = List3.List(List3.ListIndex)  'File to be deleted stored in str1
 'Example l3_assign.txt | (size) ::G:\l3\Assignment\l3_assign.txt
 
 Dim ind As Integer
 ind = List3.ListIndex     'Index is stored
 
 
 If (str1 = "") Then
 MsgBox "Please select file to be deleted form listbox"    'If file not selected
 
 Else
 'Loop to separate file name and file path
 While (fg <> 2)
 
    temp = Left(str1, k)  'to retrieve file name fom left
    
    temp2 = Right(temp, 1)  'to get rightmost element of string
    
    If (temp2 = ":") Then   'when : is found  file path starts
    fg = fg + 1
    Else
    
    End If
    
    k = k + 1
    Wend
    
    k = (Len(str1) - Len(temp))   'to store index  of start of file path
 
Set FileObject = FileSystemObject.GetFile(Right(str1, k))   'To get file path which is to be deleted
On Error GoTo Ne
 FileObject.Delete 'File deleted
 GoTo ne2
Ne:
 MsgBox " Permission Denied"
 GoTo ne3
ne2:
 MsgBox "File is deleted"
 List3.RemoveItem (ind)  'File removed from list box
 End If
ne3:
End Sub

Private Sub Form_Load()
Dim j As Integer
'To initialise list box 2 and 4 to 0
For j = 0 To 127
List2.AddItem (0)
List4.AddItem (0)
Next j

count1 = 0
  'Creating File System Object
 Set fso = CreateObject("Scripting.FileSystemObject")
 'Create an output file
Set ObjOutFile = fso.CreateTextFile("OutputFiles.csv")
  'Writing CSV headers
ObjOutFile.WriteLine ("Type,File Name,File Path,Size")
 'Call the GetFile function to get all files
 GetFiles ("G:gbc")  'Give the directories here
 
  'Close the output file
 ObjOutFile.Close
  
  'Calling required function
  retrieve_start
  search_file
 
  End Sub
  
  
  'This function helps in retrieving  start of given file in the total file
  
  Function retrieve_start()
  
  Dim i As Integer
  i = 0
  Dim k As Integer
  k = 0
  Dim l As Integer
  l = 0

  Dim p, n As Integer
  
  While i < List1.ListCount
  p = Asc(Left(List1.List(i), 1)) 'Get ascii of first letter of file
  n = List2.List(p) 'Find total number of files of that ascii
  l = Val(n)
List4.List(p) = k   'Storing starting sequence of file
  k = k + Val(l)    'Incrementing the total count of file
  i = i + l
  Wend
  End Function


'This function retrieves the file name and subfolders of the given parameter as folder name
'Syntax Function function name (parameter as folder name )
Function GetFiles(FolderName)
On Error Resume Next
   
Dim i As Integer
i = 0
Dim j As Integer
j = 0
    
    Set ObjFolder = fso.GetFolder(FolderName)       'To declare object of folder
    Set ObjFiles = ObjFolder.Files  'To declare object of files in folders

Dim s As String


For Each objfile In ObjFiles

ObjOutFile.WriteLine ("File," & objfile.Name & "," & objfile.Path & "," & objfile.Size)   'Writing file name ,size,path in file
List1.AddItem UCase(objfile.Name) & "|" & objfile.Size & " ::" & objfile.Path   'Adding files in listbox

s = UCase(objfile.Name)
'List2(ascii(Left(objfile.Name, 1))) = List2(ascii(Left(objfile.Name, 1))) + 1
i = Val(Asc(Left(s, 1)))
j = Val(List2.List(i))
List2.List(Asc(Left(s, 1))) = Val((List2.List(Asc(Left(s, 1))))) + 1
'List4.AddItem List1.List(j)
   Next

    'Getting all subfolders
    Set ObjSubFolders = ObjFolder.SubFolders
    
        For Each ObjFolder In ObjSubFolders
       
        'Writing SubFolder Name and Path
    'ObjOutFile.WriteLine ("Folder," & ObjFolder.Name & "," & ObjFolder.Path)
    'List2.AddItem ObjFolder.Name

'Getting all Files from subfolder
GetFiles (ObjFolder.Path)
    Next

'Dim o As Integer
'For o = 0 To 127
'If (Val(List2.List(o)) <> 0) Then
'List2.List(o) = "  " & List2.List(o) & " in::" & o
'End If
'Next o
 End Function


'Finding wether the file is duplicate or not

Function search_file()
Dim i As Integer
i = 0
Dim j As Integer
Dim str1, str2 As String
Dim start, end1 As Integer
Dim as1, as2 As Integer

Dim temp1, temp2, temp3 As String
Dim fg3 As Integer
fg3 = 0
Dim k, l, fg, fg1, m, tempfg As Integer
k = 0
l = 0
fg = 0
fg1 = 0
tempfg = 0

For i = 0 To List1.ListCount - 1    'For comparing each file
    fg = 0
    fg3 = 1
    fg1 = 0
    tempfg = 0
    k = 0
    j = 0
    str1 = List1.List(i)    'Sotring file name ,size,path
    
    If str1 = "" Then
    fg1 = 2
    End If
    
    as1 = Asc(Left(str1, 1))
    
    'Storing start and end of the file first letter ascii
  'We would directly search from start and end instead of searching whole file
  'Like for file  abc.txt we need to search only in a.txt to az.txt
  
  start = Val(List4.List(as1))  'From list4 we will get start for the search
  end1 = Val(List2.List(as1)) + Val(start)  'From list2 we will get end of the search
    
    'To separate file name and size from complete string
    While (fg1 <> 2)
    temp1 = Left(str1, k)
    temp2 = Right(temp1, 1)
    If (temp2 = ":") Then
    fg1 = fg1 + 1
    End If
    k = k + 1
    Wend
    
    
    While start < end1
  str2 = List1.List(start)  'File for comparing
  
  fg2 = 0
  m = 0
  temp3 = ""
  temp2 = ""
  
  If (str2 = "") Then
  fg2 = 2
  End If
  
  'To separate file name and size
  While (fg2 <> 2)
    temp3 = Left(str2, m)
    temp2 = Right(temp3, 1)
    If (temp2 = ":") Then
    fg2 = fg2 + 1
    End If
    m = m + 1
    Wend
    
  
    'If duplicate file is found and file is not same as the file to be searched
    
    If (temp1 = temp3) And start <> i Then
    
    If (tempfg = 0) Then
    List3.AddItem List1.List(i) 'Add the file in the list
    tempfg = 1 'For more than duplicate files
    End If
    
    fg = 1
    End If
    
    start = start + 1
    Wend
    
Next i



End Function




