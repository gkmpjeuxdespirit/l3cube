VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton delete_file 
      Caption         =   "Delete Duplicate file"
      BeginProperty Font 
         Name            =   "Sylfaen"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   11520
      TabIndex        =   2
      Top             =   6960
      Width           =   3615
   End
   Begin VB.ListBox List1 
      Height          =   5910
      Left            =   10440
      TabIndex        =   1
      Top             =   240
      Width           =   5415
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "Ass5.frx":0000
      Height          =   7335
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   12938
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   615
      Left            =   3360
      Top             =   5520
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   1085
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=MSDAORA.1;Password=password;User ID=system;Data Source=orcl;Persist Security Info=True"
      OLEDBString     =   "Provider=MSDAORA.1;Password=password;User ID=system;Data Source=orcl;Persist Security Info=True"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "L3_5"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim conn As ADODB.Connection
Dim rec As ADODB.Recordset
Dim rec2 As ADODB.Recordset

Dim fso
 Dim ObjFolder
Dim count1 As Integer
 Dim ObjSubFolder

    Dim ObjFiles
    Dim objfile
 
 Dim ObjOutFile
 
Private Sub Command1_Click()

End Sub

Private Sub delete_file_Click()
Dim flag As Integer
 flag = 0
 Dim k As Integer
 k = 0
 
 Dim temp, temp2 As String
 
 'Variables required to access file system,directories
 
 Dim FileSystemObject, FileObject As Object
 Set FileSystemObject = CreateObject("Scripting.FileSystemObject")
 
 FileName = List1.List(List1.ListIndex)  'File to be deleted stored in filename
 'Example l3_assign.txt | (size) ::G:\l3\Assignment\l3_assign.txt
 
 Dim ind As Integer
 ind = List1.ListIndex     'Index is stored
 
 
 If (FileName = "") Then
 MsgBox "Please select file to be deleted form listbox"    'If file not selected
 
 Else
 'Loop to separate file name and file path
 
Set FileObject = FileSystemObject.GetFile(FileName)   'To get file path which is to be deleted
On Error GoTo Ne
 FileObject.Delete 'File deleted
 GoTo ne2
Ne:
 MsgBox " Permission Denied"
 GoTo ne3
ne2:
 MsgBox "File is deleted"
 List1.RemoveItem (ind)  'File removed from list box
 
ne3:
 End If

End Sub

Private Sub Form_Load()

Set conn = New ADODB.Connection

conn.Open "Provider=MSDAORA.1;password=password;User ID=system ;Persist Security Info=False"
conn.CursorLocation = adUseClient
On Error Resume Next
conn.Execute ("drop table l3_5")    'Dropping table to update new file record
conn.Execute ("create table l3_5(name varchar(50),size_file number(30),path varchar(100))")

 Set fso = CreateObject("Scripting.FileSystemObject")
 'Create an output file
Set ObjOutFile = fso.CreateTextFile("OutputFiles.csv")
  'Writing CSV headers
ObjOutFile.WriteLine ("Type,File Name,File Path,Size")
 'Call the GetFile function to get all files
 GetFiles ("C:")  'Give the directories here
 duplicate
 
  'Close the output file
 ObjOutFile.Close

End Sub

Function GetFiles(FolderName)
On Error Resume Next
   
Dim i As Integer
i = 0
Dim j As Integer
j = 0
    
    Set ObjFolder = fso.GetFolder(FolderName)       'To declare object of folder
    Set ObjFiles = ObjFolder.Files  'To declare object of files in folders

Dim s As String


For Each objfile In ObjFiles

ObjOutFile.WriteLine ("File," & objfile.Name & "," & objfile.Path & "," & objfile.Size)   'Writing file name ,size,path in file
conn.Execute (" insert into l3_5 values('" & UCase(objfile.Name) & "'," & objfile.Size & ",'" & objfile.Path & "')")


   Next

    'Getting all subfolders
    Set ObjSubFolders = ObjFolder.SubFolders
    
        For Each ObjFolder In ObjSubFolders

'Getting all Files from subfolder
GetFiles (ObjFolder.Path)
    Next


 End Function

Function duplicate()


Set rec = New ADODB.Recordset
Set rec2 = New ADODB.Recordset
rec.Open "select * from l3_5", conn, adOpenKeyset, adLockOptimistic, adCmdText  'File database

While Not rec.EOF 'Check for each file

Set rec2 = New ADODB.Recordset
'Comparing for each file with every file present in database
        rec2.Open "select path from l3_5 where name='" & rec!Name & "' and size_file=" & rec!Size_file & " and path!='" & rec!Path & "'", conn, adOpenKeyset, adLockOptimistic, adCmdText

While Not rec2.EOF
List1.AddItem rec2!Path  'Adding duplicate file in listbox
rec2.MoveNext
Wend
rec2.Close
rec.MoveNext    'Moving to next record
Wend


End Function

